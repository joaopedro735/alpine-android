ARG JDK_VERSION
ARG CI_REGISTRY_IMAGE
ARG IMAGE_NAME
ARG TARGET_SDK

FROM ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:android-${TARGET_SDK}-jdk${JDK_VERSION}
LABEL maintainer="Álvaro Salcedo García <alvaro@alvr.dev>"

ARG BUILD_TOOLS

ENV PATH $PATH:${ANDROID_SDK_ROOT}/build-tools/${BUILD_TOOLS}:${ANDROID_SDK_ROOT}/emulator

# Install SDK Packages
ARG TARGET_SDK

RUN sdkmanager --sdk_root="${ANDROID_SDK_ROOT}" --install "emulator" "patcher;v4" "system-images;android-${TARGET_SDK};google_apis;x86_64"
CMD ["/bin/bash"]
