ARG JDK_VERSION
ARG CI_REGISTRY_IMAGE
ARG IMAGE_BASE_NAME

FROM ${CI_REGISTRY_IMAGE}/${IMAGE_BASE_NAME}:jdk${JDK_VERSION}
LABEL maintainer="Álvaro Salcedo García <alvaro@alvr.dev>"

ARG BUILD_TOOLS
ARG TARGET_SDK

ENV PATH $PATH:${ANDROID_SDK_ROOT}/build-tools/${BUILD_TOOLS}

# Install SDK Packages
RUN sdkmanager --sdk_root="${ANDROID_SDK_ROOT}" --install "build-tools;${BUILD_TOOLS}" "platforms;android-${TARGET_SDK}"

CMD ["/bin/bash"]
